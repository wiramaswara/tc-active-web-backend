var express = require('express')
var session = require('express-session');
var http = require('http');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var mongoose = require('mongoose');
var config = require('./config');
var morgan = require('morgan');

// Mongoose Connection
mongoose.connect(config.database.connection);


var app = express();
app.set('views', __dirname + '/views');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(morgan('combined'));
app.use(session({secret: 'lalalala'}));


if ('development' == app.get('env')) {
    app.use(errorHandler());
}


app.get('/', function(req, res) {
    res.redirect('/app')
});

app.use('/static', express.static(__dirname + '/static'));
app.use('/app', require('./controllers/app'))
app.use('/api/user', require('./controllers/user'));
app.use('/api/activity', require('./controllers/activity'));


app.listen(3000, "0.0.0.0");
