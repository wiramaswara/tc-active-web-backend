var mongoose = require('mongoose');
var relationship = require('mongoose-relationship')

var userSchema = new mongoose.Schema({
    userName: String,
    password: String,
    activity: [{type: mongoose.Schema.ObjectId, ref: 'Activity'}]
});

var User = mongoose.model('User', userSchema);

exports.User = User;

