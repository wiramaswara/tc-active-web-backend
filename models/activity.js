var mongoose = require('mongoose');
var relationship = require('mongoose-relationship')

var activitySchema = new mongoose.Schema({
    activityName: String,
    user: { type: mongoose.Schema.ObjectId, ref: 'User', childPath: 'activity'},
    activityData: mongoose.Schema.Types.Mixed,
});

activitySchema.plugin(relationship, {relationshipPathName: 'user'});
var Activity = mongoose.model('activity', activitySchema);

exports.Activity = Activity;

