var express = require('express');
var verifyUser = require('./library').verifyUser;
var trycatch = require('trycatch');
var unirest = require('unirest');
var backendAddress = "http://localhost:3000/api"


module.exports = (function() {
    var app = express.Router();

    var verifyAuth = function(req, res, next) {
        trycatch(function() {
            if(req.session.token) {
                unirest.post(backendAddress + '/user/verifyToken').type('json').send({token: req.session.token}).end(function(response) {
                    if(response.body.valid == true) {
                        next();
                    } else {
                        throw new Error("Token invalid");
                    }
                });
            } else {
                throw new Error("No session found");
            }

        }, function(ex) {
            res.redirect('/app/login?msg=' + ex.toString());
        })
    };

    app.get('/', function(req, res) {
        if(req.session.token) {
            res.redirect('/app/main');
        } else {
            res.redirect('/app/login');
        }
    });

    app.get('/login', function(req, res) {
        res.render('login.ejs')
    });

    app.post('/login', function(req, res) {

        trycatch(function() {
            unirest.post(backendAddress + '/user/requestToken').type('json').send({
                userName: req.body.userName,
                password: req.body.password
            }).end(function(response) {
                if(response.body.token) {
                    req.session.token = response.body.token;
                    req.session.userName = req.body.userName;
                    res.redirect('/app/main');
                } else {
                    throw new Error("Token unreachable");
                }
            });
        }, function(ex) {
            res.redirect('/app/login?msg=' + ex.toString());
        });

    });

    app.get('/main', verifyAuth, function(req, res) {
        res.render('main.ejs', {
            token: req.session.token,
            backendAddress: backendAddress,
            userName: req.session.userName
        });
    });

    app.get('/logout', verifyAuth, function(req, res) {
        req.session.destroy();
        res.redirect('/app/login');
    })

    return app;
})();
