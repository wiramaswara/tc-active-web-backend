var jwt = require('jsonwebtoken');
var tokenSecret = require('../config').tokenSecret;
var userModel = require('../models/user').User;
var trycatch = require('trycatch');

exports.verifyUser = function(req, res, next) {
    if(req.method == "GET") {
        var token = req.query.token
    } else {
        var token = req.body.token;
        delete req.body.token;
    }

    trycatch(function() {
        jwt.verify(token, tokenSecret, function(err, decoded) {
            if(err) throw(new Error('Token invalid'));
            else {
                // Verify if userName is exist
                var userName = decoded.iss;
                userModel.findOne({userName: userName}, function(err, user) {
                    if(err || user == null) throw(new Error('Token invalid'));
                    else {
                        // Verify if it is not expired
                        if(decoded.exp >= Date.now()) {
                            req.userName = decoded.iss;
                            req.userId = user._id;
                            next();
                        } else {
                            throw(new Error('Token expired'));
                        }
                    }
                });
            }
        });
    }, function(ex) {
        res.status(400);
        res.send({error: ex.toString()})
    });
}
