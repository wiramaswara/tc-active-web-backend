var express = require('express');
var trycatch = require('trycatch');
var verifyUser = require('./library').verifyUser;
var activityModel = require('../models/activity').Activity;

module.exports = (function() {
    var app = express.Router();
    app.get('/', verifyUser, function(req, res) {
        trycatch(function() {
            activityModel.find({user: req.userId}, function(err, activities) {
                if(err) throw err;
                res.status(200);
                res.send(activities);
            })
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        })
    });

    app.get('/:activity_id', verifyUser, function(req, res) {
        trycatch(function() {
            activityModel.findOne({'_id': req.params.activity_id, user: req.userId}, function(err, activity) {
                if(err) throw err;
                res.status(200);
                res.send(activity);
            })
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        });
    });

    app.post('/', verifyUser, function(req, res) {
        trycatch(function() {
            var newActivity = new activityModel(req.body);
            newActivity.user = req.userId;
            newActivity.save(function(err) {
                if(err) throw err;
                res.status(200);
                res.send(newActivity);
            });
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        });
    });
    return app;
})();
