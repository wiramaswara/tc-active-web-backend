var express = require('express');
var moment = require('moment');
var jwt = require('jsonwebtoken');
var userModel = require('../models/user').User;
var bcrypt = require('bcrypt');
var tokenSecret = require('../config').tokenSecret;
var trycatch = require('trycatch');

module.exports = (function() {
    var app = express.Router();

    app.get('/', function(req, res) {
        res.send({message: "Hello World"});
    });

    app.post('/requestToken', function(req, res) {
        var userName = req.body.userName;
        var password = req.body.password;

        trycatch(function() {
            if(userName && password) {
                // User verification
                userModel.findOne({userName: userName}, function(err, resultUser) {
                    if(err) throw err;
                    else {
                        if(resultUser == null) {
                            throw new Error('User not found');
                        } else {
                            bcrypt.compare(password, resultUser.password, function(err, result) {
                                if(!err && result == true) {
                                    var expires = moment().add('days', 7).valueOf();
                                    var token = jwt.sign({
                                        iss: userName,
                                        exp: expires
                                    }, tokenSecret);
                                    res.status(200);
                                    res.send({token: token, expires: expires, user: userName});
                                } else {
                                    throw new Error('Authentication error');
                                }
                            });
                        }
                    }
                })
            } else {
                throw new Error('You don\'t specify userName or password');
            }
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        });

    });

    app.post('/verifyToken', function(req, res) {
        var token = req.body.token;
        trycatch(function() {
            jwt.verify(token, tokenSecret, function(err, decoded) {
                if(err) throw(new Error('Token invalid'));
                else {
                    // Verify if userName is exist
                    var userName = decoded.iss;
                    userModel.findOne({userName: userName}, function(err, user) {
                        if(err || user == null) throw(new Error('Token invalid'));
                        else {
                            // Verify if it is not expired
                            if(decoded.exp >= Date.now()) {
                                res.status(200);
                                res.send({valid: true});
                            } else {
                                throw(new Error('Token expired'));
                            }
                        }
                    });
                }
            })
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        });

    });

    app.post('/register', function(req, res) {
        // Give me your userName and Password
        var userName = req.body.userName;
        var password = req.body.password;

        trycatch(function() {
            if(userName && password) {
                // Check if "userName" is exist
                userModel.findOne({userName: userName}, function(err, user) {
                    if(!err && user == null) {
                        // Do registration
                        bcrypt.genSalt(10, function(err, salt) {
                            if(err) throw(err);
                            bcrypt.hash(password, salt, function(err, hash) {
                                if(err) throw(err);
                                var newUser = new userModel({userName: userName, password: hash});
                                newUser.save(function(err) {
                                    if(err) throw(err);
                                    res.status(200);
                                    res.send({registered: true});
                                });
                            });
                        })
                    } else {
                        throw(new Error('userName had been registered, find another name for it'))
                    }
                });
            } else {
                throw(new Error('userName or password isn\'t specified'));
            }
        }, function(ex) {
            res.status(400);
            res.send({error: ex.toString()})
        });

    });

    return app;
})();
