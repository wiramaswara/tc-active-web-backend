# Dokumentasi API tc-active

## Perhatian

* Semua data harus dikirim dalam format JSON
* Semua data diterima dalam bentuk JSON
* Pastikan pengiriman data JSON dilakukan dengan menambahkan "Content-Type: application/json" pada header
* Akses ke API selain /api/user membutuhkan token, token bisa direquest via POST /api/user/requestToken dan diselipkan sebagai "token" di setiap request

## Otorisasi pengguna

### POST /api/user/register

* Tujuan: Digunakan untuk mendaftarkan pengguna baru
* Masukan:
  * {"userName": "namaUser", "password": "passwordSebelumdiHash"}
* Luaran:
  * Jika pendaftaran berhasil, {"registered": "true"} dengan kode HTTP 200
  * Jika gagal, akan ada {"error": "Pesan error"} dengan kode HTTP 400

### POST /api/user/requestToken

* Tujuan: Digunakan untuk mendapatkan token dari pengguna
* Masukan:
  * {"userName": "namaUser", "password": "passwordSebelumdiHash"}
* Luaran:
  * Jika berhasil, {"token": "token anda"} dengan kode HTTP 200
  * Jika gagal, akan ada {"error": "Pesan error"} dengan kode HTTP 400

### POST /api/user/verifyToken

* Tujuan: Digunakan untuk memverifikasi apakah token valid
* Masukan:
  * {"token": "token anda"}
* Luaran:
  * Jika berhasil, {"valid": true} dengan kode HTTP 200
  * Jika gagal, akan ada {"error": "Pesan error"} dengan kode HTTP 400

## Aktivitas

Bentuk data aktivitas adalah sebagai berikut :

{
    activityName: "Nama Aktivitas",
    activityData: [
    {
        time: "2012-04-23T18:25:43.511Z",  // Waktu pengambilan titik ini, usahakan dibuat interval
        gpsLong: "longtitude",
        gpsLat: "latitiude",
        posX: "angka",
        posY: "angka",
        posZ: "angka",
        label: "label"
    },{
        time: "2012-04-23T18:25:43.511Z",  // Waktu pengambilan titik ini, usahakan dibuat interval
        gpsLong: "longtitude",
        gpsLat: "latitiude",
        posX: "angka",
        posY: "angka",
        posZ: "angka",
        label: "label"
    },{
        time: "2012-04-23T18:25:43.511Z",  // Waktu pengambilan titik ini, usahakan dibuat interval
        gpsLong: "longtitude",
        gpsLat: "latitiude",
        posX: "angka",
        posY: "angka",
        posZ: "angka",
        label: "label"
    }]
}

### GET /api/activity?token=token
* Tujuan: Digunakan untuk melihat aktivitas dari user
* Masukan: 
  * ?token="token-anda" diselipkan dalam alamat, atau
  * {"token": "token anda"} diselipkan dalam body
* Luaran:
  * Daftar aktivitas dalam bentuk array []

### GET /api/activity/:activity_id?token=token
* Tujuan: Digunakan untuk melihat sebuah aktivitas dari user
* Masukan: 
  * {"token": "token anda"} diselipkan dalam body
* Luaran:
  * Sebuah aktivitas (bukan berbentuk array)

### POST /api/activity
* Tujuan: Digunakan untuk mendaftarkan aktivitas baru dari pengguna
* Masukan: 
  * {"token": "token anda"} diselipkan dalam body
* Luaran:
  * Sebuah aktivitas yang sudah terdapat "_id" nya



