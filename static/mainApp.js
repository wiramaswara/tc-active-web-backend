var app = angular.module("userApp", ["ngRoute"]);

app.run(function($rootScope) {
    $rootScope.token = document.getElementById("token").value;
})

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {templateUrl: '/static/list.html'});
    $routeProvider.when('/:activityId', {templateUrl: '/static/view.html'});
}]);

app.controller('activityListController', function($scope, $rootScope, $routeParams, $http) {
    $scope.getActivities = function() {
        $http.get('/api/activity?token=' + $rootScope.token).success(function(response) {
            $scope.activities = response;
        });
    }

    $scope.getActivities();
});

app.controller('activityViewController', function($scope, $rootScope, $routeParams, $http) {
    $scope.getActivity = function() {
        $http.get('/api/activity/' + $routeParams.activityId + '?token=' + $rootScope.token).success(function(response) {
            $scope.activity = response;

            // Process geo positioning
            var pos = []
            var tempPos = []
            var label = null;
            var BerlariIcon = {

            }
            var map = L.map('map').setView([-7.279209, 112.797489], 17);
            var icon = [];
            icon["Berlari"] = L.icon({
                iconUrl: '/static/Berlari.png',
                iconSize:     [50, 50],
                iconAnchor:   [22, 94]
            });
            icon["Berjalan"] = L.icon({
                iconUrl: '/static/Berjalan.png',
                iconSize:     [50, 50],
                iconAnchor:   [22, 94]
            });
            icon["Duduk"] = L.icon({
                iconUrl: '/static/Duduk.png',
                iconSize:     [50, 50],
                iconAnchor:   [22, 94]
            });
            console.log(icon);
            for(idx in $scope.activity.activityData) {
                if(label == null) {
                    label = $scope.activity.activityData[idx].label;
                    L.marker([parseFloat($scope.activity.activityData[idx].gpsLat), parseFloat($scope.activity.activityData[idx].gpsLong)], {icon: icon[label]}).addTo(map);
                    tempPos = []
                    tempPos.push([parseFloat($scope.activity.activityData[idx].gpsLat), parseFloat($scope.activity.activityData[idx].gpsLong)])
                } else if($scope.activity.activityData[idx].label != label || idx == $scope.activity.activityData.length - 1) {
                    pos.push({label: label, pos: angular.copy(tempPos)});
                    label = $scope.activity.activityData[idx].label;
                    tempPos = []
                    tempPos.push([parseFloat($scope.activity.activityData[idx].gpsLat), parseFloat($scope.activity.activityData[idx].gpsLong)])
                } else {
                    tempPos.push([parseFloat($scope.activity.activityData[idx].gpsLat), parseFloat($scope.activity.activityData[idx].gpsLong)])
                }

            }
            L.tileLayer('http://{s}.tile.openstreetmap.org//{z}/{x}/{y}.png', {
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
                maxZoom: 18
            }).addTo(map);
            console.log(pos);
            for(idx in pos) {
                var polyline = L.polyline(pos[idx].pos, {color: "red"}).addTo(map);
                polyline.bindPopup(pos[idx].label);
                map.fitBounds(polyline.getBounds());
            }

        });
    };

    $scope.getActivity();



});
