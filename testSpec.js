var unirest = require('unirest');
var faker = require('faker');
var token = null;
var siteUrl = "http://localhost:3000/api";

describe("user API and token test", function() {
    var userUserName = "tes";
    var userPassword = "tes";

    it("should able to register for new user", function(done) {
        var newUser = {
            userName: userUserName,
            password: userPassword
        };

        unirest.post(siteUrl + '/user/register').type("json").send(newUser).end(function(response) {
            expect(response.code).toBe(200);
            expect(response.body.error).not.toBeDefined();
            expect(response.body.registered).toBe(true);
            done();
        });
    });

    it("should able to get the user token", function(done) {
        var authData = {
            userName: userUserName,
            password: userPassword
        };

        unirest.post(siteUrl + '/user/requestToken').type("json").send(authData).end(function(response) {
            expect(response.code).toBe(200);
            expect(response.body.token).toBeDefined();
            token = response.body.token;
            done();
        });
    });

    it("should able to verify the token as the valid one", function(done) {
        unirest.post(siteUrl + '/user/verifyToken').type("json").send({token: token}).end(function(response) {
            expect(response.code).toBe(200);
            expect(response.body.error).not.toBeDefined();
            expect(response.body.valid).toBe(true);
            done();
        });
    });

});

describe('activity API test', function() {
    var activityId = null;

    it("should able to push new activity", function(done) {
        var newActivity = {
            activityName: faker.lorem.sentence(),
            activityData: [],
            token: token

        };

        for(var i = 0; i < 100; i++) {
            var newActivityData = {
                time: faker.date.past(),
                gpsLong: faker.address.longitude(),
                gpsLat: faker.address.latitude(),
                posX: faker.helpers.randomNumber(),
                posY: faker.helpers.randomNumber(),
                posZ: faker.helpers.randomNumber(),
                label: "running"
            }

            newActivity.activityData.push(newActivityData);
        }

        unirest.post(siteUrl + '/activity').type('json').send(newActivity).end(function(response) {
            expect(response.code).toBe(200);
            expect(response.body._id).toBeDefined();
            activityId = response.body._id;
            done();
        });
    });

});
